# Bug tracker

## Initialiser le projet

```shell
npm install
```

Compile et hot-reloads pour environnement de developpement

```shell
npm run serve
```

Compile et minification pour environnement de production

```shell
npm run build
```

## Consignes

Vous devez créer une Single Page Application permettant d'avoir un suivi des bugs sur differents projets (Recettage par exemple)

## Context du projet

* Pouvoir créer des projets : nom, description

* Pouvoir créer des tickets dans chacun des projets

* Chaque ticket devra comporter

  - Un utilisateur qui a créé le ticket
  - Un utilisateur assigné
  - Des etiquettes indiquant clairement le statut
  - Une date début/fin, si le temps est dépassé
  - Un titre
  - Une catégorie
  - Une description

* **Bonus** : Chaque ticket pourra avoir sa couleur qui change en fonction de différents paramètres (si la date limite est dépassée, code couleur en fonction de la nature du bug)

* **Bonus** : Vous aurez un espace pour lister les membres de l’équipe. Chaque utilisateur de l’équipe doit avoir une fiche dédiée qui :

  - Liste ses compétences
  - Affiche sa bio
  - Affiche son statut (displonible ou non)

  

  ## Technos

  Le projet sera en Vue JS pour le front. Tout le backend sera fait avec Firebase. Firebase va gérer la base de données, l’authentification, l’upload d’images et la mise en ligne. Il faudra mettre en ligne le projet sur Firebase Il faudra utiliser un framework CSS sans JS L’application n’a pas besoin d'être responsive : cible desktop.