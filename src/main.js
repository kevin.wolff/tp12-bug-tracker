import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase/app'
import 'firebase/firestore'
import { firestorePlugin } from 'vuefire'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

import 'vue-awesome/icons/file'
import 'vue-awesome/icons/globe'
import 'vue-awesome/icons/lock'
import 'vue-awesome/icons/pen'

const firebaseConfig = ({
  apiKey: 'AIzaSyCgrb6HNJSDD9wr5Yv_9KRLHRpyyuLtlgQ',
  authDomain: 'bug-tracker-simplon.firebaseapp.com',
  databaseURL: 'https://bug-tracker-simplon-default-rtdb.europe-west1.firebasedatabase.app/',
  projectId: 'bug-tracker-simplon',
  storageBucket: 'bug-tracker-simplon.appspot.com',
  messagingSenderId: '375378642841',
  appId: '1:375378642841:web:b6f815bff251bf26777508'
})

export const db = firebase.initializeApp(firebaseConfig).firestore()

Vue.config.productionTip = false
Vue.use(firestorePlugin)
Vue.component('v-icon', Icon)

// Trick pour éviter que l'app ne s'affiche avant d'avoir checker le user statut
// Evite qu'un user connecté, sur une page restrainte soit redirigé vers la page logIn
// car l'app est chargé avant d'avoir le statut de l'utilisateur
let app
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
})
