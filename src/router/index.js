import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase'
import Registration from '@/views/Registration'
import LogIn from '@/views/logIn'
import myProfile from '@/views/myProfile'

Vue.use(VueRouter)

const routes = [
  {
    path: '/registration',
    name: 'Registration',
    component: Registration
  },
  {
    path: '/login',
    name: 'LogIn',
    component: LogIn
  },
  {
    path: '/myprofile',
    name: 'MyProfile',
    component: myProfile,
    meta: {requiresAuth: true}
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const isAuthenticated = firebase.auth().currentUser
  if (requiresAuth && !isAuthenticated) {
    next('/login')
  }
  else {
    next()
  }
})

export default router
